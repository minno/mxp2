%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Beamer Presentation
% LaTeX Template
% Version 1.0 (10/11/12)
%
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND THEMES
%----------------------------------------------------------------------------------------

\documentclass{beamer}

\mode<presentation> {

% The Beamer class comes with a number of default slide themes
% which change the colors and layouts of slides. Below this is a list
% of all the themes, uncomment each in turn to see what they look like.

%\usetheme{default}
%\usetheme{AnnArbor}
%\usetheme{Antibes}
%\usetheme{Bergen}
%\usetheme{Berkeley}
%\usetheme{Berlin}
%\usetheme{Boadilla}
%\usetheme{CambridgeUS}
%\usetheme{Copenhagen}
%\usetheme{Darmstadt}
%\usetheme{Dresden}
%\usetheme{Frankfurt}
%\usetheme{Goettingen}
%\usetheme{Hannover}
%\usetheme{Ilmenau}
%\usetheme{JuanLesPins}
%\usetheme{Luebeck}
\usetheme{Madrid}
%\usetheme{Malmoe}
%\usetheme{Marburg}
%\usetheme{Montpellier}
%\usetheme{PaloAlto}
%\usetheme{Pittsburgh}
%\usetheme{Rochester}
%\usetheme{Singapore}
%\usetheme{Szeged}
%\usetheme{Warsaw}

% As well as themes, the Beamer class has a number of color themes
% for any slide theme. Uncomment each of these in turn to see how it
% changes the colors of your current slide theme.

%\usecolortheme{albatross}
%\usecolortheme{beaver}
%\usecolortheme{beetle}
%\usecolortheme{crane}
%\usecolortheme{dolphin}
%\usecolortheme{dove}
%\usecolortheme{fly}
%\usecolortheme{lily}
%\usecolortheme{orchid}
%\usecolortheme{rose}
%\usecolortheme{seagull}
%\usecolortheme{seahorse}
%\usecolortheme{whale}
%\usecolortheme{wolverine}

\setbeamertemplate{footline} % To remove the footer line in all slides uncomment this line
%\setbeamertemplate{footline}[page number] % To replace the footer line in all slides with a simple slide count uncomment this line

\setbeamertemplate{navigation symbols}{} % To remove the navigation symbols from the bottom of all slides uncomment this line
}

\usepackage{graphicx} % Allows including images
\usepackage{booktabs} % Allows the use of \toprule, \midrule and \bottomrule in tables

%----------------------------------------------------------------------------------------
%	TITLE PAGE
%----------------------------------------------------------------------------------------

\title{Measuring the Velocity Distribution of Cosmic Ray Muons using Cherenkov Radiation} % The short title appears at the bottom of every slide, the full title is only on the title page

\author{Ryan Thomas \& Will Fischer} % Your name
\institute[UMN] % Your institution as it will appear on the bottom of every slide, may be shorthand to save space
{
University of Minnesota \\}  % Your institution for the title page
\medskip
\date{March 12, 2014} % Date, can be changed to a custom date

\begin{document}

\begin{frame}
\titlepage % Print the title page as the first slide
\end{frame}

\begin{frame}
\frametitle{Overview} % Table of contents slide, comment this block out to remove it
\tableofcontents % Throughout your presentation, if you choose to use \section{} and \subsection{} commands, these will automatically be printed on this slide as an overview of your presentation
\end{frame}

%----------------------------------------------------------------------------------------
%	PRESENTATION SLIDES
%----------------------------------------------------------------------------------------
%------------------------------------------------
\section{Goal and Motivation}
\subsection{Goal}
\begin{frame}
\frametitle{Goal}
\huge{Goal: to measure the velocity distribution of muons using Cherenkov radiation in a pressurized tube of gas by varying the pressure and observing the resulting rate of muon detection.}
\end{frame}
%------------------------------------------------
\subsection{Motivation}
\begin{frame}
\frametitle{Motivation}
Studying muons is useful in astrophysics, as they provide a free particle beam.
\begin{itemize}
\item Can be used to calibrate instrumentation.
\item Provide information about the cosmic rays that produce them.
\end{itemize}
\end{frame}

%------------------------------------------------
\section{Theory and Background}
\subsection{Muons} % A subsection can be created just before a set of slides with a common theme to further break down your presentation into chunks
\begin{frame}
\frametitle{Cosmic Ray Muons}
\begin{itemize}
\item Muons are charged, massive, unstable particles.
\item Muons are generated in the atmosphere when cosmic rays (typically protons) strike the upper atmosphere.
\item These rays generate a shower of secondary particles, including muons.
\item These muons are generated with high velocities, very nearly the speed of light (\~0.994c for a typical muon).
\item The typical rate of muons arriving at the surface of the Earth vertically is around. $0.01 \frac{muon}{s*cm^2*sr}$. 
\end{itemize}
\end{frame}

%------------------------------------------------
\subsection{Cherenkov Radiation}
\begin{frame}
\frametitle{Cherenkov Radiation}
\begin{itemize}
\item Cherenkov radiation is caused when a charged particle travels through a medium greater than the speed of light in that medium (characterized by the index of refraction $n=\frac{c}{v}$).
\item This produces a "shockwave" of photons in front of the particle with angle $\theta$ given by the formula $\cos\theta=\frac1{n\beta}$, where $\beta=\frac{v_p}{c}$.
\end{itemize}
\begin{columns}[c]
\column{.55\textwidth}
\begin{itemize}
\item The number of photons produced over a spectral range $\lambda_1$ to $\lambda_2$ is 

$N = 2 \pi \alpha {\it l} \cdot (\frac{1}{\lambda_{2}} - \frac{1}{\lambda_{1}})\cdot(1 - \frac{1}{\beta^{2} \cdot n^{2}})$.
\item For a fully relativistic muon in air at STP, this would produce $\approx$ 30 photons per meter in the visible spectrum.
\end{itemize}
\column{.45\textwidth}
\includegraphics[scale=0.6]{cherenkov-diagram.png}
\end{columns}
\end{frame}


%------------------------------------------------
\section{Experimental Procedure}
\subsection{Setup}
\begin{frame}
\frametitle{Setup}
\begin{columns}
\column{.5\textwidth}
\begin{itemize}
\item A tube of gas is placed vertically between two scintillator panels.
\item These scintillator panels are calibrated to detect muons passing through them, allowing us to reject muons that pass through the detector horizontally.
\item The muons with a $\beta$ greater than $\frac{1}{n}$ generate photons by Cherenkov radiation as they pass between through the tube.
\item These photons are then bounced off a mirror into a photo-multiplier tube located at the bottom.
\end{itemize}
\column{.5\textwidth}
\includegraphics[width=\linewidth]{Apparatus.eps}
\end{columns}
\end{frame}

%------------------------------------------------
\subsection{Method}
\begin{frame}
\frametitle{Method}
The refractive index of a dilute gas in air is given by the Lorentz-Lorenz equation
\[
n \approx \sqrt{1+\frac{3AP}{RT}} 
\]
where A is the molar refractivity of the gas, T is the temperature, and R is the universal gas constant ($\approx$ $8.3~\frac{\mathrm{J}}{\mathrm{mol~K}}$). We can therefore change the index of refraction by increasing the pressure, detecting muons with lower velocities at higher pressures. 
Subtracting the rate of muons detected at higher pressures from those at lower pressures then gives us the integral intensity of muons with $\beta$ values between the threshold values at those pressures.
\end{frame}
%------------------------------------------------
\begin{frame}
\frametitle{Threshold Momentum}
\includegraphics[scale=0.5]{thresholdp_2.eps}
\end{frame}
%------------------------------------------------
\section{Expected Difficulties and Proposed Solutions}
\begin{frame}
\frametitle{Expected Issues}
\begin{enumerate}
\item The rate of muons hitting the detector is expected to be low (one every few minutes).
\item We are strictly limited to a maximum pressure of 150 PSI, which limits our detection range.
\item Since muons are produced by cosmic ray showers, multiple muons can strike the detector at once, a situation called a "muon shower".
\end{enumerate}
\end{frame}
%------------------------------------------------
\begin{frame}
\frametitle{Solutions}
\begin{enumerate}
\item The first can be solved by taking long periods of data (several days worth at each pressure).
\item The second can be partly solved by taking measurements at below atmospheric pressures, using a vacuum pump (which we are hoping to do if we have time).
\item The final issue can be solved by examining the pulse amplitude from the PMT. We expect a certain range in the number of photons produced by a single muon: if the pulse amplitude corresponds to more photons than could be produced by a single muon, we can reject it. We intend to try writing a machine learning algorithm to examine the individual data points to eliminate this noise. We are also intending to take background data with a black cloth covering the mirror to determined the background rate.
\end{enumerate}
\end{frame}
%------------------------------------------------
\section{Schedule and Equipment}
\begin{frame}
\frametitle{Schedule and Equipment}
\begin{columns}
\column{.5\linewidth}
\textbf{Parts}
\begin{itemize}
\item Pressure tube
\item Two scintillator plates (with attached PMTs)
\item The Cherenkov PMT
\item Discriminators and coincidence detectors
\item Gas canister
\item Digital Oscilloscope and PC
\end{itemize}
\column{.5\linewidth}
\textbf{Schedule}
\begin{itemize}
\item 3/3: Apparatus setup. 

\item 3/10: Apparatus setup and the beginning of background data collection. 

\item 3/17: Data collection (till end). 

\item 3/24: Preliminary analysis; programming for full analysis. 

\item 3/31: Programming. 

\item 4/7: Midterm report.

\item 4/14: Analysis. 

\item 4/21: Final paper and presentation preparation. 

\item 4/28: Final paper. 

\item 5/5: Finish final paper. 

\end{itemize}
\end{columns}
\end{frame}
%------------------------------------------------
\begin{frame}
\frametitle{References}
\footnotesize{
\begin{thebibliography}{9} % Beamer does not support BibTeX so references must be inserted manually as below
\bibitem{PDG} J. Beringer et al. (Particle Data Group), Phys. Rev. D86, 010001 (2012)
\end{thebibliography}
}
\end{frame}
%------------------------------------------------
\section{Conclusion}
\begin{frame}
\frametitle{Conclusion}
\begin{itemize}
\item Muons are generated at relativistic velocities by cosmic rays.
\item When they pass through a gas faster than the speed of light in that gas, they release Cherenkov radiation.
\item The speed of light in a gas is inversely proportional to the pressure.
\item We can vary the pressure in a tube and measure the resulting change in rates to determine the integral velocity spectrum of muons from cosmic rays.
\end{itemize}
\end{frame}

%------------------------------------------------

\begin{frame}
\Huge{\centerline{The End}}
\end{frame}

%----------------------------------------------------------------------------------------

\end{document} 
