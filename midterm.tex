\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{epstopdf}
\setlength{\topmargin}{-.5in}
\setlength{\textheight}{9in}
\setlength{\oddsidemargin}{.125in}
\setlength{\textwidth}{6.25in}
\setlength{\parskip}{5mm plus4mm minus3mm}
\usepackage{setspace}
\begin{document}
\title{MXP Project Midterm Report:\\
Measuring the Velocity Spectrum of Cosmic Ray Muons Using Cherenkov Radiation}
\author{Ryan Thomas \& Will Fischer \\
University of Minnesota}
\date{April 9th, 2014} 
\maketitle
\begin{abstract}
\textit{We propose to measure the integral velocity spectrum of cosmic ray muons by measuring the Cherenkov radiation produced in a tube of pressurized gas across a range of velocity thresholds. We will apply machine learning techniques to improve the background event rejection rate. We will compare the derived spectrum to that derived by other experiments, including previous MXP ones.}
\end{abstract}
\doublespacing
\section*{Introduction}
Muons are produced when high-energy cosmic rays (usually protons) strike the upper atmosphere of the Earth. This results in a shower of secondary particles, including relativistic muons. When these muons enter a medium and travel faster than the speed of light in that medium, they produce photons in a phenomenon known as Cherenkov radiation. Since speed of light in a gas is determined by its index of refraction, which changes with the gas's pressure, the speed threshold for a muon to produce Cherenkov radiation can be controlled.

We propose to use this Cherenkov radiation to measure the integral spectrum of muons incident on a tube of pressurized gas. By counting the number of muons in a given time that produce Cherenkov radiation, altering the pressure (and therefore the index of refraction), and repeating the experiment, we can determine the rate of muons that are above the threshold velocity to trigger Cherenkov radiation in the tube. With measurements at several different thresholds, the muon spectrum can be measured.

One problem that arises in this study is the possibility of muon showers. This occurs when an especially high-energy cosmic ray produces a large number of muons, causing many of them to strike the detector simultaneously. While scintillator panels can be used to verify that a muon that produces Cherenkov radiation had actually passed straight through the detector itself, if many muons strike at once, the panels and detector can be triggered by different muons, rendering that instance of data useless for the analysis. The exact rate of these showers events is unknown and will be determined as part of the experiment. We propose to use a machine learning algorithm to analyze the pulse-shape of the Cherenkov radiation detected by the photomultiplier tube (PMT) in order to distinguish between this sort of noise and useful data.

\section*{Theory}
Cherenkov radiation is produced when a particle traveling at velocity $v_{p}$ travels through a medium with index of refraction $n>\frac{c}{v_{p}}$. Defining a constant $\beta=\frac{v_{p}}{c}$, this produces a cone of light at an angle to the particle of $\cos\theta=\frac{1}{n\beta}$. 
\begin{figure}
\centering
\includegraphics[scale=0.6]{cherenkov-diagram.png}
\caption{A schematic diagram of Cherenkov radiation produced by a charged particle.}
\end{figure}
The momentum of the particle can then be found from the relativistic momentum equation
\begin{equation}\label{eq:relmomentum}
p=\frac{\beta mc}{\sqrt{1-\beta^2}}
\end{equation}
Since the mass \textit{m} of the muon is know, and given the condition that $\beta > \frac{1}{n}$ for the muon to produce Cherenkov radiation, equation \ref{eq:relmomentum} can be rewritten as 
\begin{equation}
p\ge\frac{mc}{\sqrt{n^2-1}}
\end{equation}
For a pressurized gas, the index of refraction can be determined approximately by the Lorentz-Lorenz equation as 
\begin{equation}\label{eq:index}
n\approx {\sqrt  {1+{\frac  {3AP}{RT}}}}
\end{equation}
where A is the molar refractivity of the gas, P is the pressure, R is the universal gas constant ($8.314\,4621~\frac{\mathrm{J}}{\mathrm{kmol~K}}$), and T is the temperature.

For a particle with charge \textit{z}, the differential number of photons produced per unit length at a wavelength $\lambda$ by Cherenkov radiation is given by \cite{PDG} to be
\begin{equation}
\frac{{\rm d}N}{{\rm d}x} = 2 \pi \alpha z^2 \int \left( 1 -
\frac{1}{n^2\beta^2}\right) \frac{{\rm d}\lambda}{\lambda^2}
\end{equation}
where $\alpha$ is the fine structure constant $\frac{e^2}{hc} \approx \frac{1}{137}$. Integrating this over the path length \textit{l} for the muon (charge  $-1.602*10^{-19}$C) gives a number of photons produced over the spectral range from $\lambda_1$ to $\lambda_2$ of
\begin{equation}\label{eq:numberphotons}
N = 2 \pi \alpha {\it l} \cdot (\frac{1}{\lambda_{2}} - \frac{1}{\lambda_{1}})\cdot(1 - \frac{1}{\beta^{2} \cdot n^{2}(\lambda)})
\end{equation}
Equation \ref{eq:numberphotons} assumes that insignificant energy is lost due by the particle over the path length. The loss due to Cherenkov radiation was found to be minimal by \cite{PDG}, while losses from braking effects in the gas can be ignored for particles with large ($>$ 1GeV) momentum.
\section*{Experimental Setup}
The experimental apparatus is drawn in schematic form in figure 1. The primary component in the apparatus is a stainless steel tube rated to pressures of up to 150 PSI. This tube is 1.4 meters in length, and 9.2 cm in inner diameter. This tube will be filled with pressurized carbon dioxide gas from a canister of high pressure CO2, with the pressure being set by us through a pressure regulator. Two scintillator panels with attached PMTs will be placed above and below the tube. These scintillator panels are attached to a discriminator which will be calibrated to detect primarily muons, and then to a coincidence detector. This coincidence system will both reject muons that do not pass through both panels and eliminate noise from the scintillator panels, allowing us to detect only muons that pass vertically through the tube. Eliminating non-vertical muons is important to maximize the muons' path length through the pressure tube (which will maximize the probability of detecting Cherenkov radiation).
\begin{figure}
\includegraphics[scale=0.7]{Apparatus.eps}
\caption{Experimental apparatus.}
\end{figure}

The PMT on the tube will be placed outside the tube at the bottom of the tube, looking in through a quartz glass window. The photons produced by the Cherenkov radiation are directed to this tube by means of a mirror and aluminized mylar waveguide. This placement prevents muons that pass from the tube from striking the PMT itself, which due to the glass front would produce a massive spike of Cherenkov radiation (the refractive index of the glass being much higher than the gas inside the detector).

Muons incident vertically upon the apparatus will activate the scintillator panels, which will trigger the oscilloscope to read the pulse spike from the PMT. This data is then read from the oscilloscope by the attached computer. If $\beta_{\mu}<\frac{1}{n}$, no Cherenkov radiation will be emitted by the muon, and the PMT will show a zero pulse. If $\beta_{\mu}>\frac{1}{n}$, then the PMT will show a pulse spike from the photons emitted by Cherenkov radiation as the muon passes through the pressurized gas, with the height of the pulse spike being directly proportional to the number of photons produced (as given in equation \ref{eq:numberphotons}. The spectral range and number of photons detected by the PMT is dependent on the PMT itself, and will be determined once the PMT is obtained). By varying the pressure inside the tube, we can vary the refractive index as given by equation \ref{eq:index}. 
\begin{figure}
\includegraphics[scale=0.6]{threshold.eps}
\caption{Threshold muon momentum for Cherenkov radiation to be emitted.}
\label{fig:threshold}
\end{figure}
The threshold momentum required for a photon to produce Cherenkov radiation at a given pressure is given in figure \ref{fig:threshold}. This gives an effective detection range of  3.5 $\frac{GeV}{c}$ at 15 PSI, down to 1.1 $\frac{GeV}{c}$ at 150 PSI. These constraints are determined by the design of the pressure tube. It cannot handle more than 150 PSI, nor can it be evacuated to less than 1 atmosphere of pressure.

The number of muons expected per second can be approximated from the geometry of the apparatus. The detector has a radius of 4.9 cm, and a length of $\approx$ 1.4 meters. This gives a solid angle of 0.00334 $sr$, with a detector area of 66.5 $cm^2$. Previous experiments \cite{PDG} have shown a muon intensity at sea level of $0.01 \frac{muons}{s\cdot cm^2 \cdot sr}$ which yields an intensity of 0.0023 $\frac{muons}{second}$, or about 1 every 10 minutes. 

\section*{Method}
First, the scintillator panels will be calibrated to a threshold voltage so that they detect only muons, by observing the count rate of the panels and adjusting the discriminator until the rate matches that expected (given above). Next, the tube will be pressurized with CO2 to slightly above one atmosphere of pressure, and the rate of muons that trigger the PMT will be measured. This gives the rate of muons whose momentum is higher than the threshold momentum, as given in figure \ref{fig:threshold}. Next, the pressure in the tube will be increased at regular intervals, and the rate of muons measured at each of those pressures. The momentum (and therefore velocity) distribution of the muons can then be determined simply by subtracting the rate at two different pressures.

We also expect to see a number of muon shower events, produced by multiple muons striking the detector at once. These events will produce a noise that will have to be corrected for. Two methods of eliminating this noise will be used: first, the rate of detections when a black cloth is inserted into the tube, covering the mirror, will be found experimentally. Since no photons from inside the tube will be able to hit the PMT with this cloth in place, any events observed under these conditions must be from muon shower events creating Cherenkov radiation inside the glass and PMT itself. 

Secondly, the pulse shape of individual events will be analyzed. The expected number of photons produced from a muon with a $\beta_{\mu}$ at a given pressure can be determined from equations \ref{eq:numberphotons} and \ref{eq:index}.  If multiple muons travel through the tube at once, the number of photons produced will be considerably higher (in many cases, higher than what could be produced even from a muon traveling at the speed of light). We intend to apply a machine learning algorithm using this knowledge to determine which individual events are plausibly single muon events, and which are created by a muon shower event. Several different machine learning algorithms, including neural networks and k-means clustering detection, will be tested for their ability to distinguish these types of events from each other.


\begin{thebibliography}{9}
\bibitem{PDG} J. Beringer et al. (Particle Data Group), Phys. Rev. D86, 010001 (2012)
\end{thebibliography}

\end{document}